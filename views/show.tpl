<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{.title}}</title>
    <script src="/static/js/jquery-1.10.2.js"></script>
</head>
<body>
    <iframe src="" frameborder="0"  id="showIframe"></iframe>
    <input type="hidden" name="" id="urlSign" value="{{.url}}">
    <input type="hidden" name="" id="timeToken" value="{{.token}}">
    <script >
        $(()=>{
            let startTime = new Date().getTime()
            $("#showIframe").attr("src",window.atob($("#urlSign").val())) .load(()=>{
                window.sessionStorage.setItem($("#timeToken").val(),(new Date().getTime() - startTime))
            })
        })
    </script>
</body>
</html>