function addContactObj(obj){
    getHistory()
    let Sign = $(obj).attr("data-Sign")
    let IsOnline = $(obj).attr("data-IsOnline")
    if(IsOnline == "no"){
        layer.msg("抱歉，该好友已经下线，不能互相交流了哦！")
        return 
    }else if(Sign == User.sign){
        layer.msg("抱歉，不能和自己交流哦！")
        return 
    }else{
        let has = 0 
        let n = contactList.length
        for(let i = 0; i < n; i++){
            if(contactList[i].Sign == Sign){
                has = 1;
                ContactIng = contactList[i]
                break;
            }
        }
        if(has ==0){
             ContactIng = createContactIngObj(Sign)
        }
        UpdateContactList(1)
    }
}

function getContactListIndex(Sign){
    let index = 0
    let n = contactList.length
    for(let i = 1; i < n; i++){
        if(contactList[i].Sign == Sign){
            index = i
            break
        }
    }
    //没有则创建一个
    if(index == 0){
        createContactIngObj(Sign)
        return n
    }else{
        return index
    }
}

//删除选项卡
function delContactObj(Sign){
    if(Sign == "QL"){
        contactList[0] = DefaultQlObj
        layer.msg("操作成功，但交流社区只清空聊天记录，不允许删除哦！")
    }else{
        let n       = contactList.length
        for(let i = 0; i < n; i++){
            if(contactList[i].Sign == Sign){
                if(contactList[i].Sign == ContactIng.Sign){
                    ContactIng = DefaultQlObj
                }
                //清除
                contactList.splice(i,1)
                break
            }
        }
      layer.msg("操作成功！")
    }
    UpdateContactList(1)

}
//获取在线状态
function getIsOnline(Sign){
    let n       = QLList.length
    let status  = true
    for(let i = 0; i < n; i++){
        if(QLList[i].Sign == Sign){
            status = QLList[i].IsOnline == "yes" ? true : false
            break
        }
    }
    return status
}

function getContactObj(Sign){
    let index = -1
    let n = contactList.length
    let obj ={}
    for(let i = 1; i < n; i++){
        if(contactList[i].Sign == Sign){
            index = i
            obj = contactList[i]
            break
        }
    }
    if(index == -1){
        obj = createContactIngObj(Sign)
    }
    return obj
}
function createContactIngObj(Sign){
   let o = getUserObj(Sign) 
   let obj = {
        type:Sign == "QL"?"QL":"SL",
        Sign:Sign,
        Name:o.Name,
        NoReadMsg:[],
        Msg:[],
        Header:o.Header
    }
    contactList.push(obj)
    return obj
}
function getUserObj(Sign){
    let n = QLList.length
    let obj = []
    for(let i = 0; i < n; i++){
        if(QLList[i].Sign == Sign){
            let o = QLList[i]
            obj = {
                type:"SL",
                Sign:Sign,
                Name:QLList[i].Name,
                NoReadMsg:[],
                Msg:[],
                Header:QLList[i].Header
            }
            break;
        }
    }  
    return obj
}
function ChangeContactObj(obj){
     let Sign = $(obj).attr("data-Sign")
     ContactIng = Sign == "QL"?contactList[0]:getContactObj(Sign)
     UpdateContactList(1)
}
function UpdateContactList(type){
    let html = ""
    let n    = contactList.length
    for(let i = 0;i < n; i++){

        let contact = contactList[i]
        html += `<p`
        html += ` data-Sign="${contact.Sign}" data-type="${contact.type}" onclick=ChangeContactObj(this) `
        if(contact.Sign == ContactIng.Sign){
            html += ' class="ContactIng">'
            if(type != 0){
                //切换聊天内容
                ShowContactContent(contact.Msg,0) //历史消息
                ShowContactContent(contact.NoReadMsg,1) //未读消息
                contactList[i].Msg = contact.NoReadMsg.length > 0 ?(contact.Msg).concat(contact.NoReadMsg):contact.Msg
                contactList[i].NoReadMsg = [] 
                $(".QL-title").html(contact.Name)//更改窗口标题

            }
        }else {
            html += ">"
            //追加未读消息标签
            if (contact.NoReadMsg.length > 0){
                html += "<span class='bred'>"+contact.NoReadMsg.length+"</span>"
            }
        }
        html += `<img src="${contact.Header}"/>&nbsp;&nbsp;${contact.Name}`
        html += `<span class='close' data-Sign="${contact.Sign}"></span>`
        html += `</p>`
    }
    $(".ContactList").html(html)
    //更新缓存
    window.localStorage.setItem("contactList",JSON.stringify(contactList))
    $(".ContactList .close").bind("click",function(event){
        event.stopPropagation();//阻止冒泡
        delContactObj($(this).attr("data-Sign"))
    })

}
//msgType 0 历史  1最新
function ShowContactContent(msgs,msgType){
    let n = msgs.length
    let dd = ""
    for(let i =0;i<n;i++){
        let msg = msgs[i]

        dd += '<dd class="'
        msg.Header = msg.Header ? msg.Header:'/static/header/1.jpg'
        if(msg.Sign == User.sign){
            let msgHeader = '<div class="msgHeader msgRight"><img src="'+msg.Header+'"/></div>'
            let p = '<div class="msgInfo"><p><span class="msg-time-city">'+msg.Time+'</span><span class="username">&nbsp;我&nbsp;</span></p>'
            p += '<p>'+msg.Content+'</p></div>'
            dd += 'msg_right">' + msgHeader + p + "</dd>"
        }else{
            let msgHeader = '<div class="msgHeader msgLeft"><img src="'+msg.Header+'"/></div>'
            let p = '<div class="msgInfo"><p><span class="username">&nbsp;'+msg.From+'&nbsp;</span><span class="msg-time-city">'+msg.Time+'</span></p>'
            p += '<p>'+msg.Content+'</p></div>'
            dd += 'msg_left">' + msgHeader + p + "</dd>"
        }
    }
    if(msgType == 1){
        if($('.QL').css('display') == "none"){
            layer.msg("亲爱的主人！技术交流社区您有一条新消息哦！")
        }
        $(".QL dl").append(dd)
    }else{
        $(".QL dl").html(dd)
    }

    msgScrollTOP()
}

function Sounds(type){
   let url = type == 0 ?'/static/msgSoundsAutoPlay.html':'/static/onlineSoundsAutoPlay.html'
   $(".msgSound").html('<iframe  allow="autoplay" src="'+url+'?t='+Math.random()+'" style="width:0px;height:0px;"></iframe>')
}
function msgScrollTOP(){
    let h = 0
    let n = $(".QL dl dd").length
    for(let i = 0; i < n; i++){
        h += $(".QL dl dd").eq(i).height() * 150 + 10
    }
    $(".QL dl").scrollTop(h);
}
function UserList(data){
    userNoLoginLists  = []
    let n = data.length
    let html = ""
    for(let i = 0; i < n; i++){
        if(data[i].IsOnline == "yes"){
           if ( data[i].Sign == User.sign) {
                data[i].Name = "<span class='red'>"+ data[i].Name + "</span>"
            }
            html += '<p title="在线状态" data-Sign="'+data[i].Sign+'"  data-IsOnline="'+data[i].IsOnline+'" onclick="addContactObj(this)" ><img src="'+data[i].Header+'"/>&nbsp;<span class="UserName">'+data[i].Name + '</span></p>'
        }else{
            userNoLoginLists.push(data[i])
        }
    }
  
    let n1 = userNoLoginLists.length
    for(let i = 0; i < n1; i++){
        html += '<p class="IsNotOnline" title="离线状态" data-Sign="'+userNoLoginLists[i].Sign+'"   data-IsOnline="'+userNoLoginLists[i].IsOnline+'"  onclick="addContactObj(this)" ><img src="'+userNoLoginLists[i].Header+'"/>&nbsp;<span class="UserName">'+userNoLoginLists[i].Name + '</span></p>'
    }

    html = "<h3>社区成员&nbsp;&nbsp;"+n+"/"+(n-n1)+"</h3>" + html
    $(".UserLists").html(html)
    QLList = data
    window.localStorage.setItem("QLList",JSON.stringify(QLList))
}
function setUserName(){
    if($("#UserName").val().length < 1){
        layer.msg("昵称不能为空!")
        return 
    }
    if( $(".selectedHeaderImg").length < 1) {
        layer.msg("请选择一个喜欢的头像!")
        return    
    }
    User.name = $("#UserName").val()
    User.header = $(".selectedHeaderImg").attr("src")
    User.sign = window.btoa(window.Math.random() *  window.Math.random() * 100000)
    window.localStorage.setItem("User",JSON.stringify(User))
    layer.msg("设置成功!")
    layer.close(layerIndex)

    var data = {}
    data.name = User.name
    data.header = User.header
    data.sign = User.sign
    data.type = "LoginSuccess"
    messageJson = json(data);
    ws.send(messageJson)   


    //防止清空缓存导致错乱
    ContactIng =    DefaultQlObj //这个是存放的Sign
    contactList = [
        DefaultQlObj
    ]
}

function setHeader(obj){
    $(".headerImg").removeClass("selectedHeaderImg")
    $(obj).addClass("selectedHeaderImg")
}
function showSetUserInfo(){
    layerIndex = layer.open({
        type:1,
        title:"请设置一个你喜欢的昵称和选择一个喜欢的头像",
        content:UserNameForm,
        maxWidth:(PageWidth - 100),
        maxHeight:(PageHeight - 300)
    })
}
function sendImg(obj){
    if(User.name == false){
        showSetUserInfo()
        return
    } 
    imgMsgSend($(obj).attr("src"))
    closeexpressionInfo()
}
function addNetImg(){
    if($("#addNetImgUrl").val().length < 6){
        layer.msg("图片地址有误，请检查！")
    }else{
        $(".headerList").html("<img class='headerImg' onclick='setHeader(this)' src='" + $("#addNetImgUrl").val() + "'/>" + $(".headerList").html())

    } 
}
function sendNetImg(){
    if(User.name == false){
        showSetUserInfo()
        return
    }
    if($("#netImgUrl").val().length < 6){
        layer.msg("图片地址有误，请检查！")
    }else{
        imgMsgSend($("#netImgUrl").val())
        closeexpressionInfo()
        //记忆网络图片
        let has = 0
        for(let i = 0;i < netImgHistory.length;i++){
            if(netImgHistory[i] == $("#netImgUrl").val()){
                has = 1
                break
            }
        }
        if(has == 0){
            netImgHistory.push($("#netImgUrl").val())
        }
        window.localStorage.setItem("netImgHistory",JSON.stringify(netImgHistory))
    }
}

function closeexpressionInfo(){
    $(".expressionInfo").hide("slow")
}
function imgMsgSend(img){
    let msg = `<img src="`+img+`" style="max-width:150px;"/>`
    SendMsg(msg)
}
function SendMsg(msg){
    if(ContactIng.Sign != "QL" && !getIsOnline(ContactIng.Sign)){
        layer.msg("抱歉，对方已下线，不能聊天了哦！")
        return
    }
    let Data = {};
    Data.type   = ContactIng.Sign == "QL" ? "QL":"SL";
    Data.data   = msg;
    Data.to     = ContactIng.Sign;
    Data.mtoName  = ContactIng.Sign == "QL" ? "所有人": getUserObj(ContactIng.Sign).Name
    Data.mtoHeader = ContactIng.Sign == "QL" ? "/static/header/QL.jpg": getUserObj(ContactIng.Sign).Header
    Data.name   = User.name;
    Data.header = User.header;
    Data.sign = User.sign;
    ws.send(json(Data))
    $("#msg").val('')
}
function expressionInfo(){
    let html = ''
    for(let i = 0;i < netImgHistory.length;i++){
        html += '<img src="'+netImgHistory[i]+'" onclick="sendImg(this)"/>'
    }
    for(let i = 1;i <= jpgCount;i++){
        html += '<img src="/static/biaoqing/'+i+'.jpg" onclick="sendImg(this)"/>'
    }
    for(let i = 1;i <= gifCount;i++){
        html += '<img src="/static/biaoqing/'+i+'.gif" onclick="sendImg(this)"/>'
    }
    $(".expressionList").html(html)
    $(".expressionInfo").show("slow")
}

function GoAnalysisSql(i){
    sql = $(".currentSqlBox .sqlDiv").removeClass("red")
    sql = $(".currentSqlBox .sqlDiv").eq(i).addClass("red")
    sql = $(".currentSqlBox .sqlDiv span").eq(i).html()
    sql = sql.replace(/&gt;/g,">")
    sql = sql.replace(/&lt;/g,"<")
    $(".Sql").val(sql)
    AnalysisSql()
}
function QMsgHasGet(msg){
  getHistory()  
  let has = false
//   let m   = contactList[0].Msg
//   let m1  = contactList[0].NoReadMsg
  let Msgs = contactList[0].Msg.concat(contactList[0].NoReadMsg)
  let n = Msgs.length
  for(let i =0; i < n; i++){
    if(Msgs[i].From == msg.From && Msgs[i].Sign == msg.Sign && Msgs[i].Time == msg.Time && Msgs[i].Content == msg.Content){
        has = true
        break
    }
  }
  return has
}
function QL(msg){
    if(msg.IsHistory == "yes" && lastMsgTime != 0){
        if(parseInt(new Date(msg.Time).getTime()) <= parseInt(new Date(lastMsgTime).getTime())){
            return 
        }
    }

    WriteMsg(msg)
    if(msgLists.length > 200){
        msgLists.shift() //限制消息只记录200条  防止过多慢
    }
    msgLists.push(msg)
    window.localStorage.setItem("msgLists",JSON.stringify(msgLists))
    // $(".sendBox").click()
}
function WriteMsg(msg){
    //if(lastMsgTime == 0 || lastMsgTime < msg.Time ){
        let dd = '<dd class="'
        msg.Header = msg.Header ? msg.Header:'/static/header/1.jpg'
        if(msg.Sign == User.sign){
            let msgHeader = '<div class="msgHeader msgRight"><img src="'+msg.Header+'"/></div>'
            let p = '<div class="msgInfo"><p><span class="msg-time-city">'+msg.Time+'</span><span class="username">&nbsp;我&nbsp;</span></p>'
            p += '<p>'+msg.Content+'</p></div>'
            dd += 'msg_right">' + msgHeader + p + "</dd>"
        }else{
            let msgHeader = '<div class="msgHeader msgLeft"><img src="'+msg.Header+'"/></div>'
            let p = '<div class="msgInfo"><p><span class="username">&nbsp;'+msg.From+'&nbsp;</span><span class="msg-time-city">'+msg.Time+'</span></p>'
            p += '<p>'+msg.Content+'</p></div>'
            dd += 'msg_left">' + msgHeader + p + "</dd>"
        }
        $(".QL dl").append(dd)
        //解决滚动条不随着消息的变多自动下移动问题
        msgScrollTOP()

        lastMsgTime = msg.Time //消息记录同步
        window.localStorage.setItem("lastMsgTime",lastMsgTime) //同步缓存
   
    //}
}

function getHistory() {
    if (window.localStorage.getItem("User") != undefined){
        User = JSON.parse(window.localStorage.getItem("User"))
    }
    if(window.localStorage.getItem("lastMsgTime") != undefined){
        lastMsgTime = window.localStorage.getItem("lastMsgTime")
   }
   if(window.localStorage.getItem("msgLists") != undefined){
        msgLists = JSON.parse(window.localStorage.getItem("msgLists"))
   }
   if(window.localStorage.getItem("contactList") != undefined){
        contactList = JSON.parse(window.localStorage.getItem("contactList"))
   }
   if(window.localStorage.getItem("QLList") != undefined){
      QLList = JSON.parse(window.localStorage.getItem("QLList"))
   }
}
function closeQL(obj){
    $(obj).parent().hide("slow")
}
function openQL(){
    $(".QL").show("slow")
}
function sendMsg(){
    if(User.name == false){
        showSetUserInfo()
        return
    }
    let msg =  $("#msg").val()
    if(msg.length < 1){
        layer.msg("消息不能为空!")
        return
    }
    SendMsg(msg)
}
//获取时间的毫秒数
function getTimeNum(t){
    let reg = /ms/
    let res = t.split("ms") 
    if(!reg.test(t)){
        res[0] = parseFloat(res[0]) * 1000
    }
    res[0] = parseFloat(res[0])
    return res
}
